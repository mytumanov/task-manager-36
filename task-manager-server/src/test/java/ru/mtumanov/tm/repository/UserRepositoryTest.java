package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.Test;
import ru.mtumanov.tm.api.repository.IUserRepository;
import ru.mtumanov.tm.enumerated.Role;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.model.User;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class UserRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final List<User> userList = new ArrayList<>();

    @Before
    public void initRepository() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final User user = new User();
            user.setFirstName("UserFirstName " + i);
            user.setLastName("UserLastName " + i);
            user.setMiddleName("UserMidName " + i);
            user.setEmail("user" + i + "@dot.ru");
            user.setLogin("USER" + i);
            user.setRole(Role.USUAL);
            userRepository.add(user);
            userList.add(user);
        }
    }

    @Test
    public void testFindByEmail() throws AbstractException {
        for (@NotNull final User user : userList) {
            assertEquals(user, userRepository.findByEmail(user.getEmail()));
        }
    }

    @Test(expected = AbstractException.class)
    public void testExceptionFindByEmail() throws AbstractException {
        userRepository.findByEmail("notvalidemail@dot.ru");
    }

    @Test
    public void testFindByLogin() throws AbstractException {
        for (@NotNull final User user : userList) {
            assertEquals(user, userRepository.findByLogin(user.getLogin()));
        }
    }

    @Test(expected = AbstractException.class)
    public void testExceptionFindByLogin() throws AbstractException {
        userRepository.findByLogin("NOTVALIDLOGIN");
    }

    @Test
    public void testIsEmailExist() {
        for (@NotNull final User user : userList) {
            assertTrue(userRepository.isEmailExist(user.getEmail()));
            assertFalse(userRepository.isEmailExist(user.getEmail() + user.getEmail()));
        }
    }

    @Test
    public void testIsLoginExist() {
        for (@NotNull final User user : userList) {
            assertTrue(userRepository.isLoginExist(user.getLogin()));
            assertFalse(userRepository.isEmailExist(user.getLogin() + user.getLogin()));
        }
    }
}
