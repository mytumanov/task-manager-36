package ru.mtumanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import ru.mtumanov.tm.api.repository.ITaskRepository;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.exception.entity.AbstractEntityNotFoundException;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.junit.Assert.*;

public class TaskRepositoryTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private static final String USER_ID_1 = UUID.randomUUID().toString();

    @NotNull
    private static final String USER_ID_2 = UUID.randomUUID().toString();

    @NotNull
    private static final Project projectUser1 = new Project();

    @NotNull
    private final List<Task> taskList = new ArrayList<>();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @BeforeClass
    public static void initData() {
        projectUser1.setName("Test task name");
        projectUser1.setDescription("Test task description");
        projectUser1.setUserId(USER_ID_1);
    }

    @Before
    public void initRepository() {
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName("Task name: " + i);
            task.setDescription("Task description: " + i);
            if (i <= 5)
                task.setUserId(USER_ID_1);
            else
                task.setUserId(USER_ID_2);
            if (i % 3 == 0)
                task.setProjectId(projectUser1.getId());
            taskList.add(task);
            taskRepository.add(task);
        }
    }

    @Test
    public void testAdd() {
        @NotNull final Task task = new Task();
        task.setName("Test task name");
        task.setDescription("Test task description");
        task.setUserId(USER_ID_1);

        taskList.add(task);
        taskRepository.add(USER_ID_1, task);
        @NotNull final List<Task> actualtaskList = taskRepository.findAll();
        assertEquals(taskList, actualtaskList);
    }

    @Test
    public void testClear() {
        assertNotEquals(0, taskRepository.getSize());
        taskRepository.clear();
        assertEquals(0, taskRepository.getSize());
    }

    @Test
    public void testExistById() {
        for (@NotNull final Task task : taskList) {
            assertTrue(taskRepository.existById(task.getUserId(), task.getId()));
        }
        assertFalse(taskRepository.existById(USER_ID_1, UUID.randomUUID().toString()));
    }

    @Test
    public void testFindAll() {
        @NotNull final List<Task> tasksUser1 = new ArrayList<>();
        @NotNull final List<Task> tasksUser2 = new ArrayList<>();
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1))
                tasksUser1.add(task);
            if (task.getUserId().equals(USER_ID_2))
                tasksUser2.add(task);
        }
        assertNotEquals(tasksUser1, tasksUser2);

        @NotNull final List<Task> actualtasksUser1 = taskRepository.findAll(USER_ID_1);
        @NotNull final List<Task> actualtasksUser2 = taskRepository.findAll(USER_ID_2);
        assertEquals(tasksUser1, actualtasksUser1);
        assertEquals(tasksUser2, actualtasksUser2);
    }

    @Test
    public void testFindOneById() throws AbstractEntityNotFoundException {
        for (@NotNull final Task task : taskList) {
            assertEquals(task, taskRepository.findOneById(task.getUserId(), task.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionFindOneById() throws AbstractEntityNotFoundException {
        for (int i = 0; i < 10; i++) {
            taskRepository.findOneById(USER_ID_1, UUID.randomUUID().toString());
        }
    }

    @Test
    public void testFindOneByIndex() {
        int i = 0;
        int j = 0;
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                assertEquals(task, taskRepository.findOneByIndex(USER_ID_1, i));
                i++;
            }
            if (task.getUserId().equals(USER_ID_2)) {
                assertEquals(task, taskRepository.findOneByIndex(USER_ID_2, j));
                j++;
            }
        }
    }

    @Test
    public void testGetSize() {
        assertEquals(taskList.size(), taskRepository.getSize());
    }

    @Test
    public void testRemove() throws AbstractEntityNotFoundException {
        for (@NotNull final Task task : taskList) {
            assertTrue(taskRepository.existById(task.getId()));
            taskRepository.remove(task.getUserId(), task);
            assertFalse(taskRepository.existById(task.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionRemove() throws AbstractEntityNotFoundException {
        for (int i = 0; i < 10; i++) {
            taskRepository.remove(USER_ID_1, new Task());
        }
    }

    @Test
    public void testRemoveById() throws AbstractEntityNotFoundException {
        for (@NotNull final Task task : taskList) {
            assertTrue(taskRepository.existById(task.getId()));
            taskRepository.removeById(task.getUserId(), task.getId());
            assertFalse(taskRepository.existById(task.getId()));
        }
    }

    @Test(expected = AbstractEntityNotFoundException.class)
    public void testExceptionRemoveById() throws AbstractEntityNotFoundException {
        for (int i = 0; i < 10; i++) {
            taskRepository.removeById(USER_ID_2, UUID.randomUUID().toString());
        }
    }

    @Test
    public void testRemoveByIndex() {
        for (@NotNull final Task task : taskList) {
            if (task.getUserId().equals(USER_ID_1)) {
                assertTrue(taskRepository.existById(task.getId()));
                taskRepository.removeByIndex(task.getUserId(), 0);
                assertFalse(taskRepository.existById(task.getId()));
            }
            if (task.getUserId().equals(USER_ID_2)) {
                assertTrue(taskRepository.existById(task.getId()));
                taskRepository.removeByIndex(task.getUserId(), 0);
                assertFalse(taskRepository.existById(task.getId()));
            }
        }
    }

    @Test
    public void testFindAllByProjectId() throws AbstractException {
        @NotNull final List<Task> tasksUser1 = new ArrayList<>();
        @NotNull final List<Task> tasksUser2 = new ArrayList<>();
        for (final Task task : taskList) {
            if (USER_ID_1.equals(task.getUserId()) && projectUser1.getId().equals(task.getProjectId()))
                tasksUser1.add(task);
            if (USER_ID_2.equals(task.getUserId()) && projectUser1.getId().equals(task.getProjectId()))
                tasksUser2.add(task);
        }
        assertNotEquals(tasksUser1, tasksUser2);
        assertEquals(tasksUser1, taskRepository.findAllByProjectId(USER_ID_1, projectUser1.getId()));
        assertEquals(tasksUser2, taskRepository.findAllByProjectId(USER_ID_2, projectUser1.getId()));
    }

    @Test(expected = AbstractException.class)
    public void testExceptionFindAllBytaskId() throws AbstractException {
        taskRepository.findAllByProjectId("", projectUser1.getId());
    }
}
