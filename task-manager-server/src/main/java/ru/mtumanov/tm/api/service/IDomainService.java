package ru.mtumanov.tm.api.service;

import ru.mtumanov.tm.exception.AbstractException;

public interface IDomainService {

    void loadDataBase64() throws AbstractException;

    void saveDataBase64() throws AbstractException;

    void loadDataBinary() throws AbstractException;

    void saveDataBinary() throws AbstractException;

    void loadDataJsonFasterXml() throws AbstractException;

    void saveDataJsonFasterXml() throws AbstractException;

    void loadDataJsonJaxb() throws AbstractException;

    void saveDataJsonJaxb() throws AbstractException;

    void loadDataXmlFasterXml() throws AbstractException;

    void saveDataXmlFasterXml() throws AbstractException;

    void loadDataXmlJaxb() throws AbstractException;

    void saveDataXmlJaxb() throws AbstractException;

    void loadDataYamlFasterXml() throws AbstractException;

    void saveDataYamlFasterXml() throws AbstractException;

}
