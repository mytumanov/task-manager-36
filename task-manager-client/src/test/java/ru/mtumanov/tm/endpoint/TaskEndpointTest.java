package ru.mtumanov.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.mtumanov.tm.api.endpoint.IAuthEndpoint;
import ru.mtumanov.tm.api.endpoint.IProjectEndpoint;
import ru.mtumanov.tm.api.endpoint.ITaskEndpoint;
import ru.mtumanov.tm.api.service.IPropertyService;
import ru.mtumanov.tm.dto.request.project.ProjectCreateRq;
import ru.mtumanov.tm.dto.request.task.*;
import ru.mtumanov.tm.dto.request.user.UserLoginRq;
import ru.mtumanov.tm.dto.response.project.ProjectCreateRs;
import ru.mtumanov.tm.dto.response.task.*;
import ru.mtumanov.tm.dto.response.user.UserLoginRs;
import ru.mtumanov.tm.enumerated.Status;
import ru.mtumanov.tm.marker.SoapCategory;
import ru.mtumanov.tm.model.Project;
import ru.mtumanov.tm.model.Task;
import ru.mtumanov.tm.service.PropertyService;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

@Category(SoapCategory.class)
public class TaskEndpointTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    @NotNull
    private final static IPropertyService propertyService = new PropertyService();

    @NotNull
    private final static String host = propertyService.getServerHost();

    @NotNull
    private final static String port = propertyService.getServerPort();

    @NotNull
    private final static ITaskEndpoint taskEndpoint = ITaskEndpoint.newInstance(host, port);

    @NotNull
    private final static IProjectEndpoint projectEndpoint = IProjectEndpoint.newInstance(host, port);

    @NotNull
    private final static IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(host, port);

    @NotNull
    private final static String login = "NOT_COOL_USER";

    @NotNull
    private final static String password = "Not Cool";

    @NotNull
    private List<Task> taskList;

    @NotNull
    private static String token;

    @NotNull
    private static Project project;

    @BeforeClass
    public static void login() {
        @NotNull final UserLoginRq request = new UserLoginRq(login, password);
        @NotNull final UserLoginRs response = authEndpoint.login(request);
        token = response.getToken();

        @NotNull final ProjectCreateRs projectResponse = projectEndpoint
                .projectCreate(new ProjectCreateRq(token, "TEST Project", "Test description"));
        project = projectResponse.getProject();
    }

    @Before
    public void initData() {
        taskList = new ArrayList<>();
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final TaskCreateRs response = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task " + i, "Test description " + i));
            taskList.add(response.getTask());
        }
    }

    @After
    public void clearData() {
        taskEndpoint.taskClear(new TaskClearRq(token));
    }

    @Test
    public void testTaskBindToProject() {
        @NotNull final TaskCreateRs taskResponse = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task to bind", "Test description"));

        taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, taskResponse.getTask().getId(), project.getId()));
        @NotNull final TaskShowByIdRs response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertEquals(project.getId(), response.getTask().getProjectId());
    }

    @Test
    public void testErrTaskBindToProject() {
        @NotNull final TaskCreateRs taskResponse = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task to bind", "Test description"));
        @NotNull TaskShowByIdRs response;
        @NotNull TaskBindToProjectRs bindResponse;

        bindResponse = taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, "", project.getId()));
        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertNull(response.getTask().getProjectId());
        assertFalse(bindResponse.getSuccess());
        assertNotNull(bindResponse.getMessage());

        bindResponse = taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, taskResponse.getTask().getId(), ""));
        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertNull(response.getTask().getProjectId());
        assertFalse(bindResponse.getSuccess());
        assertNotNull(bindResponse.getMessage());

        bindResponse = taskEndpoint.taskBindToProject(new TaskBindToProjectRq("", taskResponse.getTask().getId(), project.getId()));
        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertNull(response.getTask().getProjectId());
        assertFalse(bindResponse.getSuccess());
        assertNotNull(bindResponse.getMessage());
    }

    @Test
    public void testTaskChangeStatusById() {
        for (@NotNull final Task task : taskList) {
            @NotNull final TaskChangeStatusByIdRs response = taskEndpoint
                    .taskChangeStatusById(new TaskChangeStatusByIdRq(token, task.getId(), Status.IN_PROGRESS));
            assertTrue(response.getSuccess());
            assertNotEquals(task.getStatus(), response.getTask().getStatus());
        }
    }

    @Test
    public void testErrTaskChangeStatusById() {
        @NotNull TaskChangeStatusByIdRs response;

        response = taskEndpoint
                .taskChangeStatusById(new TaskChangeStatusByIdRq("", "123", Status.IN_PROGRESS));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint
                .taskChangeStatusById(new TaskChangeStatusByIdRq(token, "", Status.IN_PROGRESS));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskChangeStatusByIndex() {
        for (int i = 0; i < taskList.size(); i++) {
            @NotNull final TaskChangeStatusByIndexRs response = taskEndpoint
                    .taskChangeStatusByIndex(new TaskChangeStatusByIndexRq(token, i, Status.IN_PROGRESS));
            assertTrue(response.getSuccess());
            assertEquals(taskList.get(i).getId(), response.getTask().getId());
            assertNotEquals(taskList.get(i).getStatus(), response.getTask().getStatus());
        }
    }

    @Test
    public void testErrTaskChangeStatusByIndex() {
        @NotNull TaskChangeStatusByIndexRs response;
        response = taskEndpoint.taskChangeStatusByIndex(new TaskChangeStatusByIndexRq("", 0, Status.IN_PROGRESS));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskChangeStatusByIndex(new TaskChangeStatusByIndexRq(token, -8, Status.IN_PROGRESS));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskClear() {
        @NotNull TaskShowByIdRs taskRs;
        taskRs = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskList.get(0).getId()));
        assertNotNull(taskRs.getTask());

        @NotNull final TaskClearRs response = taskEndpoint.taskClear(new TaskClearRq(token));
        assertTrue(response.getSuccess());

        taskRs = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskList.get(0).getId()));
        assertNull(taskRs.getTask());
    }

    @Test
    public void testErrTaskClear() {
        @NotNull final TaskClearRs response = taskEndpoint.taskClear(new TaskClearRq(""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskCompleteById() {
        for (@NotNull final Task task : taskList) {
            @NotNull final TaskCompleteByIdRs response = taskEndpoint.taskCompleteById(new TaskCompleteByIdRq(token, task.getId()));
            assertEquals(Status.COMPLETED, response.getTask().getStatus());
            assertTrue(response.getSuccess());
        }
    }

    @Test
    public void testErrTaskCompleteById() {
        @NotNull TaskCompleteByIdRs response;
        response = taskEndpoint.taskCompleteById(new TaskCompleteByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskCompleteById(new TaskCompleteByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskCompleteByIndex() {
        for (int i = 0; i < taskList.size(); i++) {
            @NotNull final TaskCompleteByIndexRs response = taskEndpoint.taskCompleteByIndex(new TaskCompleteByIndexRq(token, i));
            assertTrue(response.getSuccess());
            assertEquals(Status.COMPLETED, response.getTask().getStatus());
        }
    }

    @Test
    public void testErrTaskCompleteByIndex() {
        @NotNull TaskCompleteByIndexRs response;
        response = taskEndpoint.taskCompleteByIndex(new TaskCompleteByIndexRq("", 0));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskCompleteByIndex(new TaskCompleteByIndexRq(token, -9));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskCreate() {
        @NotNull final String name = "Create task";
        @NotNull final String description = "description";
        @NotNull final TaskCreateRs response = taskEndpoint
                .taskCreate(new TaskCreateRq(token, name, description));
        assertEquals(name, response.getTask().getName());
        assertEquals(description, response.getTask().getDescription());
        assertEquals(Status.NOT_STARTED, response.getTask().getStatus());
    }

    @Test
    public void testErrTaskCreate() {
        @NotNull TaskCreateRs response;
        response = taskEndpoint.taskCreate(new TaskCreateRq("", "name", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskCreate(new TaskCreateRq(token, "", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskList() {
        @NotNull final TaskListRs response = taskEndpoint.taskList(new TaskListRq(token, null));
        assertEquals(taskList, response.getTasks());
    }

    @Test
    public void testErrTaskList() {
        @NotNull final TaskListRs response = taskEndpoint.taskList(new TaskListRq("", null));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskRemoveById() {
        for (@NotNull final Task task : taskList) {
            taskEndpoint.taskRemoveById(new TaskRemoveByIdRq(token, task.getId()));
            @NotNull final TaskShowByIdRs response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, task.getId()));
            assertNull(response.getTask());
        }
    }

    @Test
    public void testErrTaskRemoveById() {
        @NotNull TaskShowByIdRs response;
        response = taskEndpoint.taskShowById(new TaskShowByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskRemoveByIndex() {
        for (int i = 0; i < taskList.size(); i++) {
            taskEndpoint.taskRemoveByIndex(new TaskRemoveByIndexRq(token, 0));
            @NotNull final TaskShowByIdRs response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskList.get(i).getId()));
            assertNull(response.getTask());
        }
    }

    @Test
    public void testErrTaskRemoveByIndex() {
        @NotNull TaskShowByIdRs response;
        response = taskEndpoint.taskShowById(new TaskShowByIdRq("", "132"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskShowById() {
        for (@NotNull final Task task : taskList) {
            @NotNull final TaskShowByIdRs response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, task.getId()));
            assertEquals(task, response.getTask());
        }
    }

    @Test
    public void testErrTaskShowById() {
        @NotNull TaskShowByIdRs response;
        response = taskEndpoint.taskShowById(new TaskShowByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskShowByIndex() {
        for (int i = 0; i < taskList.size(); i++) {
            @NotNull final TaskShowByIndexRs response = taskEndpoint.taskShowByIndex(new TaskShowByIndexRq(token, i));
            assertEquals(taskList.get(i), response.getTask());
        }
    }

    @Test
    public void testErrTaskShowByIndex() {
        @NotNull TaskShowByIndexRs response;
        response = taskEndpoint.taskShowByIndex(new TaskShowByIndexRq("", 0));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskShowByIndex(new TaskShowByIndexRq(token, -3));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskShowByProjectId() {
        @NotNull final TaskCreateRs taskResponse = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task to unbind", "Test description"));
        @NotNull TaskShowByProjectIdRs response;
        response = taskEndpoint.taskShowByProjectId(new TaskShowByProjectIdRq(token, project.getId()));
        assertNull(response.getTasks());
        taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, taskResponse.getTask().getId(), project.getId()));
        response = taskEndpoint.taskShowByProjectId(new TaskShowByProjectIdRq(token, project.getId()));
        assertNotNull(response.getTasks());
    }

    @Test
    public void testErrTaskShowByProjectId() {
        @NotNull TaskShowByProjectIdRs response;
        response = taskEndpoint.taskShowByProjectId(new TaskShowByProjectIdRq("", project.getId()));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskShowByProjectId(new TaskShowByProjectIdRq(token, ""));
        assertTrue(response.getSuccess());
        assertNull(response.getTasks());
    }

    @Test
    public void testTaskStartById() {
        for (@NotNull final Task task : taskList) {
            @NotNull final TaskStartByIdRs response = taskEndpoint.taskStartById(new TaskStartByIdRq(token, task.getId()));
            assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
        }
    }

    @Test
    public void testErrTaskStartById() {
        @NotNull TaskStartByIdRs response;
        response = taskEndpoint.taskStartById(new TaskStartByIdRq("", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskStartById(new TaskStartByIdRq(token, ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskStartByIndex() {
        for (int i = 0; i < taskList.size(); i++) {
            @NotNull final TaskStartByIndexRs response = taskEndpoint.taskStartByIndex(new TaskStartByIndexRq(token, i));
            assertEquals(Status.IN_PROGRESS, response.getTask().getStatus());
        }
    }

    @Test
    public void testErrTaskStartByIndex() {
        @NotNull TaskStartByIndexRs response;
        response = taskEndpoint.taskStartByIndex(new TaskStartByIndexRq("", 0));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskStartByIndex(new TaskStartByIndexRq(token, -7));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskUnbindFromProject() {
        @NotNull final TaskCreateRs taskResponse = taskEndpoint.taskCreate(new TaskCreateRq(token, "TEST Task to unbind", "Test description"));
        taskEndpoint.taskBindToProject(new TaskBindToProjectRq(token, taskResponse.getTask().getId(), project.getId()));

        taskEndpoint.taskUnbindFromProject(new TaskUnbindFromProjectRq(token, taskResponse.getTask().getId(), project.getId()));
        @NotNull final TaskShowByIdRs response = taskEndpoint.taskShowById(new TaskShowByIdRq(token, taskResponse.getTask().getId()));
        assertNull(response.getTask().getProjectId());
    }

    @Test
    public void testErrTaskUnbindFromProject() {
        @NotNull TaskUnbindFromProjectRs response;
        response = taskEndpoint.taskUnbindFromProject(new TaskUnbindFromProjectRq("", "132", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUnbindFromProject(new TaskUnbindFromProjectRq(token, "", "123"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUnbindFromProject(new TaskUnbindFromProjectRq(token, "123", ""));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskUpdateById() {
        for (final Task task : taskList) {
            @NotNull final String name = task.getName() + " TEST";
            @NotNull final String description = task.getDescription() + " TEST";
            @NotNull final TaskUpdateByIdRs response = taskEndpoint.taskUpdateById(new TaskUpdateByIdRq(token, task.getId(), name, description));
            assertEquals(name, response.getTask().getName());
            assertEquals(description, response.getTask().getDescription());
        }
    }

    @Test
    public void testErrTaskUpdateById() {
        @NotNull TaskUpdateByIdRs response;
        response = taskEndpoint.taskUpdateById(new TaskUpdateByIdRq("", "123", "name", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUpdateById(new TaskUpdateByIdRq(token, "", "name", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUpdateById(new TaskUpdateByIdRq(token, "123", "", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }

    @Test
    public void testTaskUpdateByIndex() {
        for (int i = 0; i < taskList.size(); i++) {
            @NotNull final String name = taskList.get(i).getName() + " TEST";
            @NotNull final String description = taskList.get(i).getDescription() + " TEST";
            @NotNull final TaskUpdateByIndexRs response = taskEndpoint.taskUpdateByIndex(new TaskUpdateByIndexRq(token, i, name, description));
            assertEquals(name, response.getTask().getName());
            assertEquals(description, response.getTask().getDescription());
        }
    }

    @Test
    public void testErrTaskUpdateByIndex() {
        @NotNull TaskUpdateByIndexRs response;
        response = taskEndpoint.taskUpdateByIndex(new TaskUpdateByIndexRq("", 0, "name", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUpdateByIndex(new TaskUpdateByIndexRq(token, -9, "name", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());

        response = taskEndpoint.taskUpdateByIndex(new TaskUpdateByIndexRq(token, 0, "", "description"));
        assertFalse(response.getSuccess());
        assertNotNull(response.getMessage());
    }
}
