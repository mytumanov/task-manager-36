package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskUpdateByIndexRq;
import ru.mtumanov.tm.dto.response.task.TaskUpdateByIndexRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskUpdateByIndex extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Update task by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-update-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final TaskUpdateByIndexRq request = new TaskUpdateByIndexRq(getToken(), index, name, description);
        @NotNull final TaskUpdateByIndexRs response = getTaskEndpoint().taskUpdateByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
        }
    }

}
