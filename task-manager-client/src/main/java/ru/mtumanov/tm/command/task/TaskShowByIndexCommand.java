package ru.mtumanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.dto.request.task.TaskShowByIndexRq;
import ru.mtumanov.tm.dto.response.task.TaskShowByIndexRs;
import ru.mtumanov.tm.exception.AbstractException;
import ru.mtumanov.tm.util.TerminalUtil;

public class TaskShowByIndexCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String getDescription() {
        return "Show task by index";
    }

    @Override
    @NotNull
    public String getName() {
        return "task-show-by-index";
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[SHOW TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final TaskShowByIndexRq request = new TaskShowByIndexRq(getToken(), index);
        @NotNull final TaskShowByIndexRs response = getTaskEndpoint().taskShowByIndex(request);
        if (!response.getSuccess()) {
            System.out.println(response.getMessage());
            return;
        }
        showTask(response.getTask());
    }

}
