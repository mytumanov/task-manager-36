package ru.mtumanov.tm.dto.response.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.mtumanov.tm.model.Project;

@NoArgsConstructor
public final class ProjectUpdateByIndexRs extends AbstractProjectRs {

    public ProjectUpdateByIndexRs(@Nullable final Project project) {
        super(project);
    }

    public ProjectUpdateByIndexRs(@Nullable final Throwable err) {
        super(err);
    }

}