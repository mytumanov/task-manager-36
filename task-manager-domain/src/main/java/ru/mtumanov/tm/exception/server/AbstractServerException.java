package ru.mtumanov.tm.exception.server;

import org.jetbrains.annotations.NotNull;
import ru.mtumanov.tm.exception.AbstractException;

public abstract class AbstractServerException extends AbstractException {

    protected AbstractServerException() {
    }

    protected AbstractServerException(@NotNull final String message) {
        super(message);
    }

    protected AbstractServerException(@NotNull final String message, @NotNull final Throwable cause) {
        super(message, cause);
    }

    protected AbstractServerException(@NotNull final Throwable cause) {
        super(cause);
    }

    protected AbstractServerException(
            @NotNull final String message,
            @NotNull final Throwable cause,
            final boolean enableSuppression,
            final boolean writableStackTrace
    ) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

}
